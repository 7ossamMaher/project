package com.example.hossam.project.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hossam on 5/11/2017.
 */

public class SQLDataBase extends SQLiteOpenHelper {
    private SQLiteDatabase db;
    public static final int Db_version = 1;
    public static final String DB_NAME = "MyDataBase.db";
    public static final String DB_Table = "User";
    public static final String KeyId = "id";
    public static final String Name = "UserName";
    public static final String Password = "Password";
    public static final String Email = "Email";
    public static final String Authority = "Authority";
    SQLDataBase DB = null;

    public SQLDataBase(Context context) {

        super(context, DB_NAME, null, Db_version);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table User (id INTEGER PRIMARY KEY AUTOINCREMENT , " +
                "UserName text UNIQUE ,phone text , Email TEXT UNIQUE, Password TEXT ,Authority TEXT );");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists User");
        onCreate(db);
    }

    public Long CreateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues Values = new ContentValues();
        Values.put(KeyId, user.getId());
        Values.put(Name, user.getName());
        Values.put(Password, user.getPassword());
        Values.put(Email, user.getEmail());
        Values.put(Authority, user.getAuthority());
        Long Userid = db.insert(DB_Table, null, Values);
        return Userid;


    }

    public User getUser(int userid) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Sql = "Select * from " + DB_Table + "where" + KeyId + "=" + userid;
        Cursor cursor = db.rawQuery(Sql, null);
        if (cursor != null)
            cursor.moveToFirst();
        User user = new User();
        user.setId(cursor.getInt(cursor.getColumnIndex(KeyId)));
        user.setName(cursor.getString(cursor.getColumnIndex(Name)));
        user.setPassword(cursor.getString(cursor.getColumnIndex(Password)));
        user.setEmail(cursor.getString(cursor.getColumnIndex(Email)));
        user.setAuthority(cursor.getString(cursor.getColumnIndex(Authority)));

        return user;
    }

    public List<User> getalluser() {
        List<User> users = new ArrayList<>();
        String sql = "select *from " + DB_Table;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                User user1 = new User();
                user1.setId(cursor.getInt(cursor.getColumnIndex(KeyId)));
                user1.setName(cursor.getString(cursor.getColumnIndex(Name)));
                user1.setPassword(cursor.getString(cursor.getColumnIndex(Password)));
                user1.setEmail(cursor.getString(cursor.getColumnIndex(Email)));
                user1.setAuthority(cursor.getString(cursor.getColumnIndex(Authority)));
                users.add(user1);
            } while (cursor.moveToNext());
        }

        return users;
    }

    public void deleteuser(long userid) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DB_Table, KeyId + "=?", new String[]{String.valueOf(userid)});


    }

    public boolean update_stu_personal(String id, String name, String phone, String email, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("UserName", name);
        content.put("phone", phone);
        content.put("email", email);
        content.put("password", password);

        int result = db.update("User", content, "id=?", new String[]{id});
        //result >> the number of rows affected or >> = 0 if no update was done !

        if (result > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean update_teach_personal(String id, String name, String phone, String email, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("UserName", name);
        content.put("phone", phone);
        content.put("email", email);
        content.put("password", password);

        int result = db.update("User", content, "id=?", new String[]{id});
        //result >> the number of rows affected or >> = 0 if no update was done !

        if (result > 0) {
            return true;
        } else {
            return false;
        }
    }


}