package com.example.hossam.project;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.hossam.project.DataBase.SQLDataBase;
import com.example.hossam.project.student.student_personal;
import com.example.hossam.project.teacher.teacher_personal;

import java.util.ArrayList;

public class Login extends Activity implements OnClickListener {

    public static EditText L_name;
    public static EditText L_password;
    protected SQLDataBase DB = new SQLDataBase(Login.this);
    private Button L_LoginButton;
    private Button L_Cancel;
    private RadioButton L_AuthorityStudent;
    private RadioButton L_AuthorityDoctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        L_LoginButton = (Button) findViewById(R.id.L_LoginButton);
        L_LoginButton.setOnClickListener(this);
        L_Cancel = (Button) findViewById(R.id.L_Cancel);
        L_name = (EditText) findViewById(R.id.L_name);
        L_password = (EditText) findViewById(R.id.L_password);
        L_AuthorityStudent = (RadioButton) findViewById(R.id.L_AuthorityStudent);
        L_AuthorityDoctor = (RadioButton) findViewById(R.id.L_AuthorityDoctor);

        L_Cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.L_LoginButton:
                String UserName = L_name.getText().toString();
                String UserPassword = L_password.getText().toString();
                boolean invalid = false;

                if (UserName.equals("") || UserName == null) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Enter your UserName", Toast.LENGTH_SHORT).show();
                } else if (!(L_AuthorityDoctor.isChecked()) && !(L_AuthorityStudent.isChecked())) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please enter your Authority Student Or Doctor! ", Toast.LENGTH_SHORT).show();
                } else if (UserPassword.equals("") || UserPassword == null) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please enter your Password! ", Toast.LENGTH_SHORT).show();
                } else if (invalid == false) {

                    if (L_AuthorityDoctor.isChecked()) {
                        boolean validLogin = validateLogin(UserName, UserPassword, "Doctor", getBaseContext());
                        if (validLogin) {
//                            Toast.makeText(getApplicationContext(), "Welcome Doctor  ", Toast.LENGTH_SHORT).show();
                            long id = getLoginId(UserName, UserPassword, getBaseContext());
//                            Toast.makeText(getApplicationContext(), "Welcome Doctor  " + id, Toast.LENGTH_SHORT).show();

                            ArrayList<String> teacherinfo = searchByName(UserName);
                            Intent i = new Intent(this, teacher_personal.class);
                            i.putExtra("id", teacherinfo.get(0).toString());
                            i.putExtra("name", teacherinfo.get(1).toString());
                            i.putExtra("phone",teacherinfo.get(2).toString());
                            i.putExtra("email", teacherinfo.get(3).toString());
                            i.putExtra("password", teacherinfo.get(4).toString());
                            startActivity(i);

                        }
                    }
                    if (L_AuthorityStudent.isChecked()) {
                        boolean validLogin = validateLogin(UserName, UserPassword, "Student", getBaseContext());
                        if (validLogin) {
//                            Toast.makeText(getApplicationContext(), "Welcome Student ", Toast.LENGTH_SHORT).show();
//                            Toast.makeText(getApplicationContext(), UserName, Toast.LENGTH_SHORT).show();
                            ArrayList<String> studentinfo = searchByName(UserName); //
                            // Toast.makeText(getApplicationContext(), studentinfo.get(0).toString(), Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(this, student_personal.class);
                            i.putExtra("id", studentinfo.get(0).toString());
                            i.putExtra("name", studentinfo.get(1).toString());
                            i.putExtra("phone",studentinfo.get(2).toString());
                            i.putExtra("email", studentinfo.get(3).toString());
                            i.putExtra("password", studentinfo.get(4).toString());

                            startActivity(i);
                        }
                        finish();
                    }

                    break;
                }

        }
    }

    private boolean validateLogin(String username, String password, String Authority, Context baseContext) {
        DB = new SQLDataBase(getBaseContext());
        SQLiteDatabase db = DB.getReadableDatabase();

        String[] columns = {"id"};

        String selection = "UserName=? AND Password=? AND Authority=?";
        String[] selectionArgs = {username, password, Authority};

        Cursor cursor = null;
        try {

            cursor = db.query(DB.DB_Table, columns, selection, selectionArgs, null, null, null);
            startManagingCursor(cursor);
        } catch (Exception e)

        {
            e.printStackTrace();
        }
        int numberOfRows = cursor.getCount();

        if (numberOfRows <= 0) {

            Toast.makeText(getApplicationContext(), "User Name and Password  miss match.. OR Choose Right Authority  \nPlease Try Again", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getBaseContext(), Login.class);
            startActivity(intent);
            return false;
        }


        return true;

    }

    public void onDestroy() {
        super.onDestroy();
        DB.close();
    }

    private long getLoginId(String username, String password, Context baseContext) {
        String Name = "UserName";
        String User = "User";
        DB = new SQLDataBase(getBaseContext());
        SQLiteDatabase db = DB.getReadableDatabase();
        Cursor cursor = null;
        String Query = "SELECT  id FROM User WHERE " + Name + " = '" + username + "'";
        int id = -1;
        if (cursor != null && cursor.moveToFirst())
            id = cursor.getInt(cursor.getInt(0));
        return id;

    }

    public ArrayList searchByName(String name) {
        ArrayList arrayList = new ArrayList();
        String User = "User";
        DB = new SQLDataBase(getBaseContext());
        SQLiteDatabase db = DB.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + User + " where UserName=? ", new String[]{name});
        cursor.moveToFirst();

        while (cursor.isAfterLast() == false) {

            String t1 = cursor.getString(0);
            String t2 = cursor.getString(1);
            String t3 = cursor.getString(2);
            String t4 = cursor.getString(3);
            String t5 = cursor.getString(4);

            arrayList.add(t1);
            arrayList.add(t2);
            arrayList.add(t3);
            arrayList.add(t4);
            arrayList.add(t5);

            cursor.moveToNext();
        }
        return arrayList;
    }



    public void Go_register(View view) {
        Intent i = new Intent(this, Register.class);
        startActivity(i);
    }
}