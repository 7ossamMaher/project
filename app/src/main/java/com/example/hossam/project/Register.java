package com.example.hossam.project;

/**
 * Created by hossam on 5/11/2017.
 */

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import com.example.hossam.project.DataBase.SQLDataBase;
import com.example.hossam.project.DataBase.User;


public class Register extends Activity implements OnClickListener{
    protected SQLDataBase DB = new SQLDataBase(Register.this);
    private Button RigsterButton;
    private Button LoginButton;
    private Button Cancel;
    private EditText name;
    private EditText password;
    private EditText Email, phone;
    private RadioButton AuthorityStudent;
    private RadioButton AuthorityDoctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        RigsterButton = (Button) findViewById(R.id.RigsterButton);
        RigsterButton.setOnClickListener(this);

//        LoginButton.setOnClickListener(this);
        Cancel = (Button) findViewById(R.id.Cancel);
        name = (EditText) findViewById(R.id.name);
        phone=(EditText)findViewById(R.id.phone);
        password = (EditText) findViewById(R.id.password);
        Email = (EditText) findViewById(R.id.Email);
        AuthorityStudent = (RadioButton) findViewById(R.id.AuthorityStudent);
        AuthorityDoctor = (RadioButton) findViewById(R.id.AuthorityDoctor);


        Cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.RigsterButton:
                String UserName = name.getText().toString();
                String UserPassword = password.getText().toString();
                String UserEmail = Email.getText().toString();
                String UserPhone =phone.getText().toString();
                boolean invalid = false;

                if (UserName.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Enter your UserName", Toast.LENGTH_SHORT).show();
                }

               else if (UserPhone.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Enter your Phone", Toast.LENGTH_SHORT).show();
                }
                else if (UserEmail.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please enter your UserEmail", Toast.LENGTH_SHORT).show();
                } else if (UserPassword.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please enter your password", Toast.LENGTH_SHORT).show();
                }
                else if (CHECKName(UserName)==true){

                    invalid =true;
                    Toast.makeText(this, "user is already exist", Toast.LENGTH_SHORT).show();

                }

                else if (!(AuthorityDoctor.isChecked()) && !(AuthorityStudent.isChecked())) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please enter your Authority Student Or Doctor! ", Toast.LENGTH_SHORT).show();
                } else if (invalid == false) {

                    if (AuthorityDoctor.isChecked()) {
                        // CreateUser    (UserName, UserPassword, UserEmail,"Doctor");
                        User user = new User();
                        user.setName(UserName);
                        user.setPhone(UserPhone);
                        user.setEmail(UserEmail);
                        user.setPassword(UserPassword);
                        user.setAuthority("Doctor");
                        CreateUser(user);
                    }
                    if (AuthorityStudent.isChecked()) {
                        // CreateUser (UserName, UserPassword, UserEmail,"Student");
                        User user = new User();
                        user.setName(UserName);
                        user.setPhone(UserPhone);
                        user.setEmail(UserEmail);
                        user.setPassword(UserPassword);
                        user.setAuthority("Student");
                        CreateUser(user);
                    }
                    finish();
                }

                break;
        }

    }

    private void CreateUser(String UserName, String phone,String Password, String Email, String Authority) {

        SQLiteDatabase db = DB.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("UserName", UserName);
        values.put("phone",phone);
        values.put("Password", Password);
        values.put("Email", Email);
        values.put("Authority", Authority);

        try {
            db.insert(DB.DB_Table, null, values);

            Toast.makeText(getApplicationContext(), "your details submitted Successfully...", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Long CreateUser(User user) {
        SQLiteDatabase db = DB.getWritableDatabase();
        ContentValues Values = new ContentValues();
        Values.put("UserName", user.getName());
        Values.put("phone",user.getPhone());
        Values.put("Password", user.getPassword());
        Values.put("Email", user.getEmail());
        Values.put("Authority", user.getAuthority());
        Long Userid = null;
        try {
            Userid = db.insert(DB.DB_Table, null, Values);

            Toast.makeText(getApplicationContext(), "your details submitted Successfully...", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Userid;


    }

    public boolean CHECKName(String name) {
        String User ="User";
        DB = new SQLDataBase(getBaseContext());
        SQLiteDatabase db = DB.getReadableDatabase();

        Cursor cursor = db.rawQuery("select UserName from " + User + " where UserName=? ", new String[]{name});
        Boolean flag = false;
        if (cursor.moveToNext()) {
            flag = true;
        }
        return flag;
    }



}
