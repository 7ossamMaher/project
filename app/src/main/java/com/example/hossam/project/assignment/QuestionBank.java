package com.example.hossam.project.assignment;

import android.content.Context;

import com.example.hossam.project.database.dbhelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hossam on 11/05/17.
 */

public class QuestionBank {


    // declare list of Question objects
    List<Question> list = new ArrayList<>();
    dbhelper myDataBaseHelper;

    // method returns number of questions in list
    public int getLength() {
        return 5;
    }

    // method returns question from list based on list index
    public String getQuestion(int a) {
        return list.get(a).getQuestion();
    }

    // method return a single multiple choice item for question based on list index,
    // based on number of multiple choice item in the list - 1, 2, 3 or 4
    // as an argument
    public String getChoice(int index, int num) {
        return list.get(index).getChoice(num - 1);

    }

    //  method returns correct answer for the question based on list index
    public String getCorrectAnswer(int a) {
        return list.get(a).getAnswer();
    }


    public void initQuestions(Context context) {
        myDataBaseHelper = new dbhelper(context);
        list = myDataBaseHelper.getAllQuestionsList();//get questions/choices/answers from database

        if (list.isEmpty()) {//if list is empty, populate database with default questions/choices/answers
            myDataBaseHelper.addInitialQuestion(new Question("How many sizes are supported by Android??",
                    new String[]{"Android supported all sizes ", "Android does not support all sizes\n" +
                            "\n", "Android supports small,normal, large and extra-large sizes ", "Size is undefined in android\n"}, "Android supports small,normal, large and extra-large sizes "));

            myDataBaseHelper.addInitialQuestion(new Question("How to stop the services in android?",
                    new String[]{" finish()", "system.exit()", " By manually", "stopSelf() and stopService()"}, "stopSelf() and stopService()"));

            myDataBaseHelper.addInitialQuestion(new Question(" How to upgrade SQlite the database from a lower version to higher version in android SQlite?",
                    new String[]{" Using helper Class", "Using cursor", "Using intent", "None of the above"}, "Using helper Class"));

            myDataBaseHelper.addInitialQuestion(new Question("What is the 9 patch tool in android?",
                    new String[]{" Using with tool, we can redraw images in 9 sections", "image extension tool", "image editable tool", " Device features"}, "Using with tool, we can redraw images in 9 sections"));


            myDataBaseHelper.addInitialQuestion(new Question(" What does httpclient.execute() returns in android?",
                    new String[]{"Http entity", "Http response", " Http result", "None of the above"}, "Http response"));


            myDataBaseHelper.addInitialQuestion(new Question("How to find the JSON element length in android JSON?",
                    new String[]{"count()", "sum()", " add()", "length()"}, "length()"));


            myDataBaseHelper.addInitialQuestion(new Question("What is an interface in android?",
                    new String[]{"Interface acts as a bridge between class and the outside world", "Interface is a class", "Interface is a layout file", "None of the above"}, "Interface acts as a bridge between class and the outside world"));

            list = myDataBaseHelper.getAllQuestionsList();//get list from database again

        }
    }

}
