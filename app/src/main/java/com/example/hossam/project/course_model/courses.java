package com.example.hossam.project.course_model;


public class courses {

    static String name;
    static int number;
    static int point;
    static String semester;

    public static String getSemester() {
        return semester;
    }

    public static void setSemester(String semester) {
        courses.semester = semester;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        courses.name = name;
    }

    public static int getNumber() {
        return number;
    }

    public static void setNumber(int number) {
        courses.number = number;
    }

    public static int getPoint() {
        return point;
    }

    public static void setPoint(int point) {
        courses.point = point;
    }
}
