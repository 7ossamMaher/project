package com.example.hossam.project.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.hossam.project.assignment.Question;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class dbhelper extends SQLiteOpenHelper {
    private static final String Dbname = "data.db";

    public dbhelper(Context context) {
        super(context, Dbname, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {



        db.execSQL("create table student (id INTEGER PRIMARY KEY AUTOINCREMENT , name text , email TEXT UNIQUE, password TEXT ,phone INTEGER UNIQUE );");

        db.execSQL("create table teacher (id INTEGER PRIMARY KEY AUTOINCREMENT ,name text , number INTEGER ,email TEXT UNIQUE, password TEXT ,phone INTEGER UNIQUE);");

        db.execSQL("CREATE TABLE course( id INTEGER PRIMARY KEY	  AUTOINCREMENT ,name text, number INTEGER ,point INTEGER,semester text )");

        db.execSQL("CREATE TABLE QuestionBank (id INTEGER PRIMARY KEY AUTOINCREMENT ,question text , choice1 text , choice2 text ,choice3 text ,choice4 text,answer text  )");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("drop table if exists student");
        db.execSQL("drop table if exists teacher");
        db.execSQL("drop table if exists course");
        db.execSQL("drop table if exists assignment");
        db.execSQL("drop table if exists QuestionBank");

        onCreate(db);

    }

    // questions
    public long addInitialQuestion (Question question) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Creating content values
        ContentValues values = new ContentValues();
        values.put("question", question.getQuestion());
        values.put("choice1", question.getChoice(0));
        values.put("choice2", question.getChoice(1));
        values.put("choice3",  question.getChoice(2));
        values.put("choice4",  question.getChoice(3));
        values.put("answer", question.getAnswer());
        // insert row in question table
        long insert = db.insert("QuestionBank", null, values);
        return insert;
    }


   //  To extract data from database and save it Arraylist of data type Question

    public List<Question> getAllQuestionsList() {
        List<Question> questionArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM QuestionBank" ;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all records and adding to the list
        if (c.moveToFirst()) {
            do {
                Question question = new Question();
                String questText= c.getString(c.getColumnIndex("question"));
                question.setQuestion(questText);

                String choice1Text= c.getString(c.getColumnIndex("choice1"));
                question.setChoice(0,choice1Text);

                String choice2Text= c.getString(c.getColumnIndex("choice2"));
                question.setChoice(1,choice2Text);

                String choice3Text= c.getString(c.getColumnIndex("choice3"));
                question.setChoice(2,choice3Text);

                String choice4Text= c.getString(c.getColumnIndex("choice4"));
                question.setChoice(3,choice4Text);

                String answerText= c.getString(c.getColumnIndex("answer"));
                question.setAnswer(answerText);

                // adding to Questions list
                questionArrayList.add(question);
            } while (c.moveToNext());
            Collections.shuffle(questionArrayList);
        }
        return questionArrayList;
    }

    public boolean insertcourse(String name, int number,int point ,String semester ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("name", name);
        content.put("number",number);
        content.put("point", point);
        content.put("semester",semester);
        long result = db.insert("course", null, content);

        // result > the row ID of the newly inserted row, or -1 if an error occurred
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }



    public ArrayList getcourseData() {
        ArrayList arrayList = new ArrayList();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from course" , null);
        //Cursor object is positioned before the first entry.
        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            int number = cursor.getInt(cursor.getColumnIndex("number"));
            int point =cursor.getInt(cursor.getColumnIndex("point"));
            String semster = cursor.getString(cursor.getColumnIndex("semester"));

            arrayList.add(id + " - "+ name);

        }
        return arrayList;
    }


    public boolean removecourse(String id) {
// Firstly searching for the id
        SQLiteDatabase db1 = this.getReadableDatabase();
        Cursor cursor = db1.rawQuery("select * from course  where id=? ", new String[]{id});
        Boolean flag = false; // not founded as a default result
        if (cursor.moveToNext()) {
            flag = true;  // founded
        }
        if (flag == true) {
// continue the delete process
            SQLiteDatabase db = this.getWritableDatabase();
            int result = db.delete("course", "id=?", new String[]{id});
            // result >>= the number of rows affected , >> = 0 if No Deletion occur .
            // To remove all rows , pass "1" as the whereClause.
//            int result = db.delete(Tablename, "1",null); // all rows are deleted from table
            return true;
        } else {
            return false;
        }
    }

    public boolean updatecourse(String id ,String name, String number,String point ,String semester) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("name", name);
        content.put("number",number);
        content.put("point", point);
        content.put("semester",semester);
        int result = db.update("course", content, "id=?", new String[]{id});
        //result >> the number of rows affected or >> = 0 if no update was done !
        if (result > 0) {
            return true;
        } else {
            return false;
        }
    }




}