package com.example.hossam.project.student;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hossam.project.Login;
import com.example.hossam.project.R;
import com.example.hossam.project.assignment.Quiz;
import com.example.hossam.project.course_model.courses;
import com.example.hossam.project.database.dbhelper;

import java.util.ArrayList;
import java.util.List;

public class course_stu extends AppCompatActivity {

    dbhelper helper ;
    Button assignment;
    TextView coname ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.course_stu);
        helper = new dbhelper(this);

        assignment= (Button) findViewById(R.id.btn_assignment);
        coname = (TextView)findViewById(R.id.course_name);



    }
    //btn assigment
    public void assigment(View view) {
        Intent i = new Intent(course_stu.this,Quiz.class);
        startActivity(i);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar
        getMenuInflater().inflate(R.menu.logout, menu); //Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent i = new Intent(getApplicationContext(), Login.class);
                Login.L_name.setText("");
                Login.L_password.setText("");
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
