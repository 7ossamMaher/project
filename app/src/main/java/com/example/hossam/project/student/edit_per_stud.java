package com.example.hossam.project.student;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hossam.project.DataBase.SQLDataBase;
import com.example.hossam.project.Login;
import com.example.hossam.project.R;
import com.example.hossam.project.database.dbhelper;

public class edit_per_stud extends AppCompatActivity {

    EditText edit_id,edit_name , edit_phone , edit_email ,edit_pass ;

    Button save_info ;
    protected SQLDataBase DB = new SQLDataBase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_per_stud);



       save_info = (Button) findViewById(R.id.btn_save_info);
        edit_id =(EditText) findViewById(R.id.edit_id);
        edit_name=(EditText) findViewById(R.id.edit_name);
        edit_phone=(EditText)findViewById(R.id.edit_phone);
        edit_email=(EditText)findViewById(R.id.edit_email);
        edit_pass=(EditText)findViewById(R.id.edit_pass);

       edit_id.setEnabled(false);

        Intent i =getIntent();
        edit_id.setText(i.getStringExtra("id"));
         edit_name.setText(i.getStringExtra("name"));
         edit_phone.setText(i.getStringExtra("phone"));
         edit_email.setText(i.getStringExtra("email"));
         edit_pass.setText(i.getStringExtra("pass"));

    }

    public void btn_save(View view) {


        String id =edit_id.getText().toString();
        String name = edit_name.getText().toString();
        String phone = edit_phone.getText().toString();
        String email = edit_email.getText().toString();
        String password =edit_pass.getText().toString();


        if (id.isEmpty() || name.isEmpty() || email .isEmpty() ||
                password.isEmpty() || phone.isEmpty()) {
            Toast.makeText(edit_per_stud.this, "EditText fields are Empty !", Toast.LENGTH_SHORT).show();

        } else {
            boolean status = DB.update_stu_personal(id, name, phone,email,password);
            if (status == true) {

                Toast.makeText(edit_per_stud.this, " Update data is Done successfully .", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(edit_per_stud.this, " Update data is Failed !", Toast.LENGTH_SHORT).show();

            }
        }

        Intent in = new Intent();
        in.putExtra("rename",edit_name.getText().toString());
        in.putExtra("rephone",edit_phone.getText().toString());
        in.putExtra("reemail",edit_email.getText().toString());
        in.putExtra("repass",edit_pass.getText().toString());
        setResult(RESULT_OK,in);
        finish();

    }



    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar
        getMenuInflater().inflate(R.menu.logout, menu); //Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent i = new Intent(getApplicationContext(), Login.class);
                Login.L_name.setText("");
                Login.L_password.setText("");
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    }

//    public void update_stu_personal(View view) {
//
//        if (edit_id.getText().toString().isEmpty() || edit_name.getText().toString().isEmpty() ||
//                edit_phone.getText().toString().isEmpty()||
//                edit_email.getText().toString().isEmpty() || edit_pass.getText().toString().isEmpty()) {
//            Toast.makeText(this, "EditText fields are Empty !", Toast.LENGTH_SHORT).show();
//
//        } else {
//            boolean status = helper.update_stu_personal(edit_id.getText().toString(), edit_name.getText().toString(),
//                    edit_phone.getText().toString(),edit_email.getText().toString(),edit_pass.getText().toString());
//            if (status == true) {
//
//                Toast.makeText(this, " Update data is Done successfully .", Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(this, " Update data is Failed !", Toast.LENGTH_SHORT).show();
//
//            }
//        }
//    }



