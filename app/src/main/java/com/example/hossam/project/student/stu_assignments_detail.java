package com.example.hossam.project.student;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.hossam.project.R;

public class stu_assignments_detail extends AppCompatActivity {

    Button howto ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stu_assignments_detail);

        howto= (Button) findViewById(R.id.btn_howto);
        howto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(stu_assignments_detail.this, "Click download Button to download ", Toast.LENGTH_SHORT).show();
            }
        });

    }
}