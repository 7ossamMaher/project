package com.example.hossam.project.student;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.example.hossam.project.Login;
import com.example.hossam.project.R;
import com.example.hossam.project.database.dbhelper;


import java.util.ArrayList;

public class student_personal extends AppCompatActivity {

    dbhelper helper;
    ListView listView ;

    EditText stu_id , stu_name,stu_phone,stu_email,stu_pass;
    ArrayAdapter adapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_personal);
        helper = new dbhelper(this);

        stu_id =(EditText)findViewById(R.id.stu_id);
        stu_name=(EditText) findViewById(R.id.stu_name);
        stu_phone=(EditText) findViewById(R.id.stu_phone);
        stu_email=(EditText) findViewById(R.id.stu_email);
        stu_pass=(EditText) findViewById(R.id.stu_pass);
        listView = (ListView) findViewById(R.id.stu_list_course);


//        ArrayList arrayList = helper.getcourseData();
//        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,arrayList);


        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        String name =intent.getStringExtra("name");
        String phone = intent.getStringExtra("phone");
        String email = intent.getStringExtra("email");
        String password = intent.getStringExtra("password");



        stu_id.setText(id);
        stu_name.setText(name);
        stu_phone.setText(phone);
        stu_email.setText(email);
        stu_pass.setText(password);

        listView.setAdapter(adapter);

        showData();




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if(position==1){
                    Intent i =new Intent(student_personal.this,course_stu.class);
                    startActivity(i);
                }

            }

        });

    }

    public void edit_stu_info(View view) {

        Intent i =new Intent(student_personal.this,edit_per_stud.class);
        i.putExtra("id",stu_id.getText().toString());
        i.putExtra("name",stu_name.getText().toString());
        i.putExtra("phone",stu_phone.getText().toString());
        i.putExtra("email",stu_email.getText().toString());
        i.putExtra("pass",stu_pass.getText().toString());
        startActivityForResult(i,1);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==1){
            if (resultCode==RESULT_OK){

                stu_name.setText(data.getStringExtra("rename"));
                stu_email.setText(data.getStringExtra("reemail"));
                stu_phone.setText(data.getStringExtra("rephone"));
                stu_pass.setText(data.getStringExtra("repass"));
                Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();

            }
        }

    }

    public void showData() {

        ArrayList arrayList = helper.getcourseData();


        if (arrayList.isEmpty()) {
            Toast.makeText(this, "Database is Empty !", Toast.LENGTH_SHORT).show();
            // the two line make the listview empty due to the empty arraylist make the adapter take no value and set listview as empty listview
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);
            listView.setAdapter(arrayAdapter);

        } else {
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);
            listView.setAdapter(arrayAdapter);

        }


    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar
        getMenuInflater().inflate(R.menu.logout, menu); //Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent i = new Intent(student_personal.this, Login.class);
                Login.L_name.setText("");
                Login.L_password.setText("");
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}