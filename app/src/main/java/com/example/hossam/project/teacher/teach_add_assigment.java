package com.example.hossam.project.teacher;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import java.util.Calendar;;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.example.hossam.project.R;

public class teach_add_assigment extends AppCompatActivity {

    EditText starttime , startdate ,endtime , enddate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teach_add_assigment);

        starttime = (EditText) findViewById(R.id.write_assignment_starttime);
        startdate = (EditText) findViewById(R.id.write_assignment_startdate);
        endtime = (EditText) findViewById(R.id.write_assignment_endtime);
        enddate = (EditText) findViewById(R.id.write_assignment_enddate);



        //time picker for starttime of assignment
        starttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
              int  mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(teach_add_assigment.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                starttime.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

            }
        });


        endtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                int hour=c.get(Calendar.HOUR_OF_DAY);
                int min = c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(teach_add_assigment.this,
                        new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        endtime.setText(hourOfDay+":"+minute);
                    }
                },hour,min,false);
                timePickerDialog.show();


            }
        });



    }

}
