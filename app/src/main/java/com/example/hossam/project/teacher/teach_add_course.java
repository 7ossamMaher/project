package com.example.hossam.project.teacher;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hossam.project.Login;
import com.example.hossam.project.R;
import com.example.hossam.project.database.dbhelper;

import java.util.ArrayList;

public class teach_add_course extends AppCompatActivity {

    dbhelper helper ;
    EditText name , number , point , semester ;
   // ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teach_add_course);

        helper = new dbhelper(this);

        name=(EditText)findViewById(R.id.write_course_name);
        number = (EditText)findViewById(R.id.write_course_number);
        point=(EditText)findViewById(R.id.write_course_point);
        semester=(EditText)findViewById(R.id.write_course_semester);
        //listView = (ListView) findViewById(R.id.listcourse);

//        showData();


    }

//    public void showData() {
//
//        ArrayList arrayList = helper.getcourseData();
//
//            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);
//            listView.setAdapter(arrayAdapter);
//
//          //  Toast.makeText(this, "add ", Toast.LENGTH_SHORT).show();
//
//
//    }
//
    public void addcourse(View view) {

        String cname = name.getText().toString();
        int cnumber = Integer.parseInt(number.getText().toString());
        int cpoint = Integer.parseInt(point.getText().toString());
        String csemster = semester.getText().toString();
        boolean status = helper.insertcourse(cname,cnumber,cpoint,csemster);
        if(status == true){
            Toast.makeText(teach_add_course.this, "adding  Successfully", Toast.LENGTH_SHORT).show();
            name.setText("");
            number.setText("");
            point.setText("");
            semester.setText("");
           // showData();
        }else {
            Toast.makeText(teach_add_course.this, "Adding data is failed ! ", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar
        getMenuInflater().inflate(R.menu.logout, menu); //Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent i = new Intent(getApplicationContext(), Login.class);
                Login.L_name.setText("");
                Login.L_password.setText("");
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
