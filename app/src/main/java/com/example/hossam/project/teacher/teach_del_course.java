package com.example.hossam.project.teacher;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hossam.project.Login;
import com.example.hossam.project.R;
import com.example.hossam.project.database.dbhelper;

public class teach_del_course extends AppCompatActivity {

    dbhelper helper;
    EditText id ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teach_del_course);
        helper = new dbhelper(this);

        id = (EditText) findViewById(R.id.edit_del_course);
    }

    public void removecourse(View view) {

        if (id.getText().toString().isEmpty()) {
            Toast.makeText(this, "ID field is Empty !", Toast.LENGTH_SHORT).show();

        } else {
            boolean status = helper.removecourse(id.getText().toString());
            if (status == true) {
                Toast.makeText(this, " Delete done successfully", Toast.LENGTH_SHORT).show();
                id.setText("");

            } else {
                Toast.makeText(this, " Not Founded in Database ", Toast.LENGTH_SHORT).show();
                id.setText("");
            }
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar
        getMenuInflater().inflate(R.menu.logout, menu); //Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent i = new Intent(getApplicationContext(), Login.class);
                Login.L_name.setText("");
                Login.L_password.setText("");
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
