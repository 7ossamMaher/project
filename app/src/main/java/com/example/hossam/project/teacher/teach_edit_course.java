package com.example.hossam.project.teacher;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hossam.project.Login;
import com.example.hossam.project.R;
import com.example.hossam.project.database.dbhelper;

public class teach_edit_course extends AppCompatActivity {

    dbhelper helper ;
    EditText id , name , number , point , semester;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teach_edit_course);
        helper = new dbhelper(this);

        id = (EditText)findViewById(R.id.id_edit_course);
        name = (EditText)findViewById(R.id.update_course_name);
        number = (EditText)findViewById(R.id.update_course_number);
        point = (EditText)findViewById(R.id.update_course_point);
        semester = (EditText)findViewById(R.id.update_course_semester);


    }

    public void update_course(View view) {

            if (id.getText().toString().isEmpty() || name.getText().toString().isEmpty() ||
                    number.getText().toString().isEmpty()||
                point.getText().toString().isEmpty() || semester.getText().toString().isEmpty()) {
                Toast.makeText(this, "EditText fields are Empty !", Toast.LENGTH_SHORT).show();

            } else {
                boolean status = helper.updatecourse(id.getText().toString(), name.getText().toString(),
                        number.getText().toString(),point.getText().toString(),semester.getText().toString());
                if (status == true) {
                    id.setText("");
                    name.setText("");
                    number.setText("");
                    point.setText("");
                    semester.setText("");

                    Toast.makeText(this, " Update data is Done successfully .", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, " Update data is Failed !", Toast.LENGTH_SHORT).show();

                }
            }
        }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar
        getMenuInflater().inflate(R.menu.logout, menu); //Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent i = new Intent(getApplicationContext(), Login.class);
                Login.L_name.setText("");
                Login.L_password.setText("");
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    }

