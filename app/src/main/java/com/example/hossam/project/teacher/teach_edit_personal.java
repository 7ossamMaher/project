package com.example.hossam.project.teacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hossam.project.DataBase.SQLDataBase;
import com.example.hossam.project.Login;
import com.example.hossam.project.R;
import com.example.hossam.project.database.dbhelper;

public class teach_edit_personal extends AppCompatActivity {


    EditText teach_edit_id,teach_edit_name , teach_edit_phone , teach_edit_email
            ,teach_edit_pass ,teach_edit_address; ;
    Button teach_save_info ;
    protected SQLDataBase DB = new SQLDataBase(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tech_edit_personal);

        teach_save_info = (Button) findViewById(R.id.btn_teach_save_info);
        teach_edit_id =(EditText)findViewById(R.id.teach_edit_id);
        teach_edit_name=(EditText) findViewById(R.id.teach_edit_name);
        teach_edit_phone=(EditText)findViewById(R.id.teach_edit_phone);
        teach_edit_email=(EditText)findViewById(R.id.teach_edit_email);
        teach_edit_pass=(EditText)findViewById(R.id.teach_edit_pass);

        teach_edit_id.setEnabled(false);

        //get intent from teacher_personal
        Intent i =getIntent();
        teach_edit_id.setText(i.getStringExtra("id"));
        teach_edit_name.setText(i.getStringExtra("name"));
        teach_edit_phone.setText(i.getStringExtra("phone"));
        teach_edit_email.setText(i.getStringExtra("email"));
        teach_edit_pass.setText(i.getStringExtra("pass"));

    }
    public void update_stu_personal(View view) {

        if ( teach_edit_name.getText().toString().isEmpty() ||
                teach_edit_phone.getText().toString().isEmpty()||
                teach_edit_email.getText().toString().isEmpty() || teach_edit_pass.getText().toString().isEmpty()) {
            Toast.makeText(this, "EditText fields are Empty !", Toast.LENGTH_SHORT).show();

        } else {
            boolean status = DB.update_teach_personal(teach_edit_id.getText().toString()
                    , teach_edit_name.getText().toString(),
                    teach_edit_phone.getText().toString(),teach_edit_email.getText().toString(),
                    teach_edit_pass.getText().toString());
            if (status == true) {

                Toast.makeText(this, " Update data is Done successfully .", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, " Update data is Failed !", Toast.LENGTH_SHORT).show();

            }
        }

        Intent in = new Intent();
        in.putExtra("rename",teach_edit_name.getText().toString());
        in.putExtra("rephone",teach_edit_phone.getText().toString());
        in.putExtra("reemail",teach_edit_email.getText().toString());
        in.putExtra("repass",teach_edit_pass.getText().toString());
        //return result to teacher_personal activity
        setResult(RESULT_OK,in);
        finish();

    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar
        getMenuInflater().inflate(R.menu.logout, menu); //Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent i = new Intent(getApplicationContext(), Login.class);
                Login.L_name.setText("");
                Login.L_password.setText("");
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
