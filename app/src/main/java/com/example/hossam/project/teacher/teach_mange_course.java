package com.example.hossam.project.teacher;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hossam.project.Login;
import com.example.hossam.project.R;
import com.example.hossam.project.database.dbhelper;

import java.util.ArrayList;

public class teach_mange_course extends AppCompatActivity {

    dbhelper helper;
    ListView listView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teach_mange_course);
        helper = new dbhelper(this);

        listView = (ListView) findViewById(R.id.showcourses);

        showData();

    }

    public void toaddcourse(View view) {
        Intent i = new Intent(teach_mange_course.this,teach_add_course.class);
        startActivity(i);
    }

    public void todelcourse(View view) {

        Intent i =new Intent(teach_mange_course.this,teach_del_course.class);
        startActivity(i);
    }

    public void refresh(View view) {
        showData();
    }

    public void edit_course(View view) {
        Intent i =new Intent(teach_mange_course.this , teach_edit_course.class);
        startActivity(i);
    }

    public void showData() {

        ArrayList arrayList = helper.getcourseData();

        if (arrayList.isEmpty()) {
            Toast.makeText(this, "Database is Empty !", Toast.LENGTH_SHORT).show();
            // the two line make the listview empty due to the empty arraylist make the adapter take no value and set listview as empty listview
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);
            listView.setAdapter(arrayAdapter);
        } else {
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);

            listView.setAdapter(arrayAdapter);

        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar
        getMenuInflater().inflate(R.menu.logout, menu); //Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent i = new Intent(getApplicationContext(), Login.class);
                Login.L_name.setText("");
                Login.L_password.setText("");
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}