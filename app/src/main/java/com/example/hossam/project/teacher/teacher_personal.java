package com.example.hossam.project.teacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hossam.project.Login;
import com.example.hossam.project.R;

public class teacher_personal extends AppCompatActivity {

    Button  mange_course;
    EditText teach_id,teach_name, teach_phone, teach_email, teach_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teacher_personal);

        mange_course=(Button)findViewById(R.id.btn_teach_mange_course);
        teach_id=(EditText)findViewById(R.id.teach_id);
        teach_name = (EditText) findViewById(R.id.teach_name);
        teach_phone = (EditText) findViewById(R.id.teach_phone);
        teach_email = (EditText) findViewById(R.id.teach_email);
        teach_pass = (EditText) findViewById(R.id.teach_pass);

 //get info from database and display in the profile
        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        String name =intent.getStringExtra("name");
        String phone = intent.getStringExtra("phone");
        String email = intent.getStringExtra("email");
        String password = intent.getStringExtra("password");

        teach_id.setText(id);
        teach_name.setText(name);
        teach_phone.setText(phone);
        teach_email.setText(email);
        teach_pass.setText(password);



    }


    //action btn edit information
    public void information_tech(View view) {

        Intent i = new Intent(teacher_personal.this, teach_edit_personal.class);
        i.putExtra("id", teach_id.getText().toString());
        i.putExtra("name", teach_name.getText().toString());
        i.putExtra("phone", teach_phone.getText().toString());
        i.putExtra("email", teach_email.getText().toString());
        i.putExtra("pass", teach_pass.getText().toString());
        startActivityForResult(i, 2);

    }

    //action btn manage course
    public void mange_course(View view) {

        Intent i = new Intent(teacher_personal.this, teach_mange_course.class);
        startActivity(i);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //get the result from teach_edit_personal
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                teach_name.setText(data.getStringExtra("rename"));
                teach_email.setText(data.getStringExtra("reemail"));
                teach_phone.setText(data.getStringExtra("rephone"));
                teach_pass.setText(data.getStringExtra("repass"));
                Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();

            }
        }

    }


    // for menu "logout"
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar
        getMenuInflater().inflate(R.menu.logout, menu); //Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent i = new Intent(getApplicationContext(), Login.class);
                Login.L_name.setText("");
                Login.L_password.setText("");
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
